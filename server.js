import http from "http";
import dotenv from "dotenv";
import app from "./app.js";

dotenv.config()
const port = process.env.PORT || 5000

http.createServer(app).listen(port,() => {
    console.log(`Running on PORT ${port}`);
});
