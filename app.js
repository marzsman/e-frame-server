import dotenv from "dotenv";
import express from 'express';
import Image from "./pages/Image.js";
import Wiki from "./pages/Wiki.js";

dotenv.config()
const app = express();

const debug = process.env.DEBUG;
const width = process.env.SCREEN_WIDTH;
const height = process.env.SCREEN_HEIGHT;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.set('views', './views')
app.set('view engine', 'ejs')
app.use(express.static('public'))

app.get('/',(req,res) => {
    res.send('check');
})

app.get('/image', async (req,res) => {
    const image = await Image({width, height});
    res.setHeader('Cache-Control', 'no-cache');
    res.setHeader('Content-Type', 'image/png');
    res.end(image);
})

app.get('/wiki', async (req,res) => {
    let lang = req.query.lang ?? 'en';
    const wiki = await Wiki({lang});
    res.render('wiki', {wiki, width, height, debugSize: debug});
    //res.json({...wiki});
})

export default app;