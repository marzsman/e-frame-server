#!/usr/bin/python
# -*- coding:utf-8 -*-
import sys
import os
from urllib.request import urlopen
import json
import logging
from waveshare_epd import epd7in5_V2
import time
from PIL import Image,ImageDraw,ImageFont
import traceback
import wget

picdir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'wiki_img')

# request a new image
url = 'https://e-frame-app.vercel.app/api/image?path=https%3A%2F%2Fe-frame-app.vercel.app%2Fwiki?lang=nl'
# response = urlopen(url)

filename = os.path.join(picdir, 'image.png')
if os.path.exists(filename):
        os.remove(filename)
wget.download(url, filename)

logging.basicConfig(level=logging.DEBUG)

try:
    epd = epd7in5_V2.EPD()

    logging.info("init and Clear")
    epd.init()
    # epd.Clear()

    logging.info("Read PNG file")
    logging.info(epd.width)
    logging.info(epd.height)
    Himage2 = Image.new('1', (epd.width, epd.height), 255)  # 255: clear the frame
    bmp = Image.open(filename)
    bmp = bmp.convert("1")
    (width, height) = (bmp.width // 2, bmp.height // 2)
    bmp = bmp.resize((width, height))

    logging.info(bmp.width)
    logging.info(bmp.height)
    logging.info(round(epd.width / 2 - bmp.height / 2))
    Himage2.paste(bmp, (round(epd.width / 2 - bmp.width / 2),round(epd.height / 2 - bmp.height / 2)))
    epd.display(epd.getbuffer(Himage2))
    time.sleep(3)

    # logging.info("Clear...")
#     epd.init()
#     epd.Clear()

    logging.info("Goto Sleep...")
    epd.sleep()

except IOError as e:
    logging.info('error')
    logging.info(e)

except KeyboardInterrupt:
    logging.info("ctrl + c:")
    epd7in5_V2.epdconfig.module_exit()
    exit()
