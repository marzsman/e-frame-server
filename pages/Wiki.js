import getWikiPage from "../libs/wiki.js";

const Wiki = async ({lang}) => {
    try {
        const wikiPage = await getWikiPage({lang});

        return { ...wikiPage };
    } catch (e) {
        console.log(e);
    }
}

export default Wiki;
