import playwright from 'playwright';

const Image = async ({width, height}) => {

    const browser = await playwright.chromium.launch({
        args: ['--disable-font-subpixel-positioning','--font-render-hinting=none'],
        executablePath: process.env.CHROMIUM_EXECUTABLE
    });

    const page = await browser.newPage({
        viewport: {
            width:parseInt(width),
            height:parseInt(height),
            deviceScaleFactor: 1
        }
    });
    await page.goto(`localhost:5000/wiki?lang=${process.env.WIKI_LANG}`);
    await page.waitForSelector('#wiki');

    const img = await page.screenshot({clip: {x:0, y:0, width: parseInt(width), height: parseInt(height)}, path: './wiki_img/image.png' });

    await browser.close();

    return img;
};

export default Image;