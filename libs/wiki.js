import wiki from 'wikipedia';
import { stripHtml } from 'string-strip-html';

const getWikiPage = async ({lang = 'en'}) => {
  try {
    wiki.setLang(lang);
    const randomPage = await wiki.random('mobile-sections-lead');
    const id = randomPage.id;
    const wikiPage = await wiki.page(id);
    const summary = await wikiPage.summary();

    const parsedText = stripHtml(summary.extract_html, {
      onlyStripTags: ['a', 'img'],
      stripTogetherWithTheirContents: [
        //"script", // default
        'style' // default
        //"xml", // default
        //"pre", // <-- custom-added
      ]
    }).result;

    return {
      id,
      title: summary.title,
      url: wikiPage.fullurl,
      text: parsedText
    };
  } catch (error) {
    return error;
    //=> Typeof wikiError
  }
};

export default getWikiPage;
